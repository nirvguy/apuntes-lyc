PDF:=latexmk
PDF_ARGS:=-e '$$hash_calc_ignore_pattern{"pdf"} = "^/CreationDate |^/ModDate |^/ID \\[<";' -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make
OBJ:=Apuntes_LyC.pdf Apuntes_LyC_sin_demos.pdf
M4:=m4
GARBAGE=*.aux *.log *.fls *.fdb_latexmk *.toc

all: $(OBJ)

%.pdf: %.tex
	$(eval TEMP_FILE_DEMO:=$(shell mktemp temp_XXXXX.tex))
	$(M4) -D DEMOS $< > $(TEMP_FILE_DEMO)
	$(PDF) $(PDF_ARGS) $(TEMP_FILE_DEMO) -shell-escape
	mv $(TEMP_FILE_DEMO:%.tex=%.pdf) $@
	rm $(TEMP_FILE_DEMO)
	rm -rf $(GARBAGE)
	
%_sin_demos.pdf: %.tex
	$(eval TEMP_FILE_SIN_DEMO:=$(shell mktemp temp_XXXXX.tex))
	$(M4) $< > $(TEMP_FILE_SIN_DEMO)
	$(PDF) $(PDF_ARGS) $(TEMP_FILE_SIN_DEMO) -shell-escape
	mv $(TEMP_FILE_SIN_DEMO:%.tex=%.pdf) $@
	rm $(TEMP_FILE_SIN_DEMO)
	rm -rf $(GARBAGE)

run: all
	evince $(OBJ)

clean:
	rm -rf *.pdf $(GARBAGE)

.PHONY: clean
