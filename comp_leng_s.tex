\providecommand{\lengS}[0]{\mathcal{S}}
\subsection{Lenguaje $\lengS$}

En esta subsección se define el lenguaje imperativo de programación $\lengS$. Un lenguaje igual de poderoso que una maquina de turing.

\begin{definicion}
	$\lengS$ es el lenguaje que se compone por
	\begin{itemize}
		\item Tiene infinitas variables que su valor esta en $\Nat$
		\begin{itemize}
			\item $X_1,\dots,X_n,\dots$ son las variables de entrada de un programa de $\lengS$
			\item $Z_1,\dots,Z_n,\dots$ son las variables temporales de $\lengS$
			\item $Y$ la única variable de salida
			\item No hay mas variables
			\item Todas las variables se inicializan en $0$ implicitamente
		\end{itemize}
		\item Tiene 3 instrucciones. 
		\begin{enumerate}
			\item $V \assign V+1$ que incrementa en $1$ a $V$
			\item $V \assign V-1$ que decrementa en $V$ en $1$ si $V \neq 0$, lo deja en $0$ de lo contrario.
			\item $\textbf{IF} \hspace*{0.15cm} V \neq 0 \hspace*{0.15cm} \textbf{GOTO} \hspace*{0.15cm} L$ donde $L$ es una etiqueta
		\end{enumerate}
		\item Cada instrucción puede llevar previamente una etiqueta o no
	\end{itemize}
\end{definicion}

\begin{observacion}
	Se puede simular $V \assign 0$ con $\lengS$ de la siguiente manera
	\begin{Scode}
		\iS{[A] V \assign V - 1}
		\IFGOTO{V \neq 0}{A}
	\end{Scode}
\end{observacion}

\begin{observacion}
	Se puede simular \textbf{GOTO} $L$ con $\lengS$ de la siguiente manera
	\begin{Scode}
		\iS{Z \assign Z + 1}
		\IFGOTO{Z \neq 0}{L}
	\end{Scode}
\end{observacion}

\begin{observacion}
	Se puede simular $V \assign W$
\end{observacion}

\begin{observacion}
	Si bien $\lengS$ no tiene llamada a subrutinas estas se pueden
	simular con macros renombrando todas las variables
	temporales que no se usan en el programa que realiza la llamada
\end{observacion}


\begin{definicion}
	Sea $P$ un programa en $\lengS$, denotamos a
	$\deffuncion{\Psi_P}{\Nat^n}{\Nat}$ la función tal que
	$$ \Psi_P(x_1,\dots,x_n) = \left\lbrace\begin{array}{ll}
	                              \mbox{Estado final de la variable } Y & \mbox{si el programa $P$} \\ 
								 & \mbox{ tiene un estado final (termina) para esa entrada } x_0,\dots,x_n\\
	                              \uparrow & \mbox{si no (el programa no tiene estado de aceptacion final} \\ 
								  & \mbox{ para esa entrada, es decir el programa "se cuelga")}
	\end{array}\right.$$
\end{definicion}

\begin{notacion}
	Dada una funcion parcial $\deffuncion{f}{\Nat^n}{\Nat}$, notamos 
	$$f(x_1,\dots,x_n) = \uparrow \mbox{ ó } f(x_1,\dots,x_n) \uparrow \mbox{ si } x_1,\dots,x_n \not\in Dom(f) $$
	$$f(x_1,\dots,x_n) \downarrow \mbox{ si } x_1,\dots,x_n \in Dom(f) $$
\end{notacion}

\begin{definicion}
	Una función $\deffuncion{f}{\Nat^n}{\Nat}$ se dice
	\textbf{parcial computable} si existe un programa $P$ en $\lengS$ 
	que computa la función, esto es 
	$$ \begin{array}{llcl}
	        \Psi_P(x_1,\dots,x_n) & = & f(x_1,\dots,x_n) & \mbox{ si } x_1,\dots,x_n \in Dom(f) \\
	        \Psi_P(x_1,\dots,x_n) & = & \uparrow & \mbox{ si } f(x_1,\dots,x_n) \uparrow
		\end{array}$$
	y decimos que \textbf{computable} si la funcion $f$ es total
\end{definicion}

\begin{teorema}
	Sean $\deffuncion{f}{\Nat^k}{\Nat}$ y $\deffuncion{g_1,\dots,g_k}{\Nat^n}{\Nat}$ funciones parciales computables (computables)
	es parcial computable (computable) la función $\deffuncion{h}{\Nat^n}{\Nat}$ definida por composición de $f,g_1,\dots,g_k$ como
	$$ h(x_1, \dots, x_n) =  f(g_1(x_1,\dots,x_n),\dots,g_k(x_1,\dots,x_n)) $$
\begin{demo}
	Como $f,g_1,\dots,g_k$ son computables existen programas $F,G_1,\dots,G_K$ que computan esas funciones
	Queremos computar $h$, para eso armo el siguiente programa $H$
	\begin{Scode}[$H(X_1,\dots,X_n)$]
		\iS{Z_1 \assign G_1(X_1,\dots,X_n)}
		\iS{Z_2 \assign G_2(X_1,\dots,X_n)}
		\iS{\vdots}
		\iS{Z_k \assign G_k(X_1,\dots,X_n)}
		\iS{Y \assign F(Z_1,\dots,Z_k)}
	\end{Scode}
	En el caso de computable hay que ver que $H$ termine para toda entrada (pues en ese caso $h$ es total). 
	Esto es cierto ya que programa $H$ siempre termina pues terminan
	$F,G_1,\dots,G_k$ para toda entrada (por ser $f,g_1,\dots,g_k$ computables,
	en particular totales) 
\end{demo}
\end{teorema}

\begin{observacion}
	Tanto la clase de funciones computables como la de parciales computable es cerrada por composición
\end{observacion}

\begin{teorema}
 Sean $\deffuncion{g}{\Nat^{n+2}}{\Nat}$ y 
$\deffuncion{f}{\Nat^{n}}{\Nat}$ parcial computables (computable).
Entonces la función $\deffuncion{h}{\Nat^{n+1}}{\Nat}$ que se obtiene por 
recursión primitiva de $f,g$. Esto es,
\begin{eqnarray*}
h(x_1,\dots,x_n,0) & = & f(x_1,\dots,x_n) \\
h(x_1,\dots,x_n,t+1) & = & g(h(x_1,\dots,x_n,t),x_1,\dots,x_n,t)
\end{eqnarray*}
es parcial computable (computable)
\begin{demo}
	Sea $F$ y $G$ programas que computan las funciones $f$ y $g$ respectivamente.
	El programa $H$ que computa la función $h$ es la siguiente
	\begin{Scode}[$H(X_1,\dots,X_n,X_{n+1})$]
		\iS{Y \assign F(0,X_1,\dots,X_n)}
		\iS{Z_1 \assign 0}
		\iS{Z_2 \assign X_{n+1}}
		\IFGOTO[A]{Z_2=0}{E}
		\iS{Y \assign G(Y,X_1,\dots,X_n,Z_1)}
		\iS{Z_1 \assign Z_1+1}
		\iS{Z_2 \assign Z_2-1}
		\GOTO{A}
	\end{Scode}
\end{demo}
En el caso de computables, como este programa termina
siempre que $F$ y $G$ no se "cuelguen". El program $H$ termina
cuando $f$ y $g$ son computables (en particular totales)
\end{teorema}

\begin{observacion}
	Tanto la clase de funciones computables como la de parciales computable es cerrada por recursión primitiva
\end{observacion}

\begin{corolario}
	Tanto la clase de funciones computables como la de parciales computables es una clase $PRC$.
\end{corolario}

\begin{observacion}
	Toda funcion primitiva recursiva esta en toda clase $PRC$ por el teorema. Luego está
	en la clase de funciones computables (ya que las primitivas recursivas son totales). \\
	Es decir, si $f$ es primitva recursiva, $f$ es computable.
\end{observacion}

\begin{teorema}
	Sea $P$ un predicado $n+1$-ádico parcial computable, entonces la función $\deffuncion{\min}{\Nat^{n}}{\Nat}$
	definida como
		$$ \min(x_1,\dots,x_n) = \min_{i\in\Nat} P(i,x_1,\dots,x_n) $$
	es parcial computable
\begin{demo}
	El programa que computa parcialmente $\min$ es el siguiente programa
	\begin{Scode}[$MIN(X_1,\dots,X_n)$]
		\iS{Y \assign 0}
		\IFGOTO[A]{P(Y,X_1,\dots,X_n)}{E}
		\iS{Y \assign Y + 1}
		\GOTO{A}
	\end{Scode}
\end{demo}
\end{teorema}
